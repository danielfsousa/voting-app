variable location {
  default = "East US"
}

variable resource_group_name {
  default = "dev"
}

variable cluster_name {
  default = "demo"
}

variable dns_prefix {
  default = "demo-dns"
}

variable vm_size {
  default = "Standard_B2ms" // 8GB mem
}

variable vm_count {
  default = 2
}
