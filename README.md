# k8s-devops-tutorial

A tutorial on how to create a complex cloud native infrastructure for a simple app

## Why

My goal was to create a cloud native infrastructure that is replicalbe, secure, observable, scalable, resilient for a simple express voting app. I've used this repo mainly to sharpen my devops and kubernetes skills.

## Dependencies

- [Docker](https://www.docker.com/get-started)
- [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [Kubectx](https://github.com/ahmetb/kubectx/#installation)
- [Helm](https://helm.sh/docs/intro/quickstart/#install-helm)
- [Helmfile](https://github.com/roboll/helmfile#installation)
- [Terraform](https://www.terraform.io/downloads.html)

## Features

- Auto scalable [Kubernetes](https://kubernetes.io/) cluster
- Insfrastructure provision with [Terraform](https://www.terraform.io/)
- Cluster monitoring with [Kubernetes dashboard](https://github.com/kubernetes/dashboard)
- Logging with [Elasticsearch](https://www.elastic.co/pt/elasticsearch/) and [Kibana](https://www.elastic.co/pt/kibana)
- [Filebeat](https://www.elastic.co/pt/beats/) or [Fluentbit](https://fluentbit.io/) as a log collector
- Monitoring with [Prometheus](https://prometheus.io/) and [Grafana](https://grafana.com/)
- Application performance monitoring with [Elastic APM](https://www.elastic.co/pt/apm)
- CI/CD with [Gitlab](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/) or [Github Actions](https://github.com/features/actions) or Jenkins
- [Knative](https://knative.dev/) or [OpenFaas](https://www.openfaas.com/)
- Local development with [docker-compose](https://docs.docker.com/compose/)
- Debugging with [VSCode](https://code.visualstudio.com/docs/editor/debugging)

## Getting started

### Running locally

docker-compose

debug with vscode

minukube

### Install dependencies

First, download the apps that we will be using to follow this tutorial. You can check theirs installation instructions in the the dependencies section above.

But if you are using MacOS, you can simply install them with Homebrew:

```sh
brew cask install docker
brew install kubectl kubectx helm helmfile terraform
```

### Setup cluster

I am going to use [Azure]() for this demo, you can skip this section if you are using another cloud provider.


