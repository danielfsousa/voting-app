const pinoExpress = require('express-pino-logger')
const { nanoid } = require('nanoid')
const { logger, elasticApm } = require('./utils')

function loggerMiddleware (req, res, next) {
  logger.debug({ ns: 'loggerMiddleware' }, `currentTraceIds: ${elasticApm.currentTraceIds}`)

  const middleware = pinoExpress({
    logger: logger.child({
      ...elasticApm.currentTraceIds,
      ns: 'http'
    }),
    genReqId: () => nanoid()
  })

  middleware(req, res, next)
}

module.exports = { loggerMiddleware }
