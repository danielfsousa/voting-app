const express = require('express')
const redis = require('./redis')
const config = require('./config')
// const { logger } = require('./utils')
const { appInsights, elasticApm } = require('./utils')
const router = express.Router()

router.get('/', function (req, res, next) {
  Promise.all([redis.get(config.BUTTON1), redis.get(config.BUTTON2)])
    .then(([value1, value2]) => {
      res.render('index', {
        title: config.TITLE,
        button1: config.BUTTON1,
        button2: config.BUTTON2,
        value1,
        value2
      })
    })
    .catch(next)
})

router.post('/', async function (req, res, next) {
  try {
    const { vote } = req.body
    req.log.info({ ns: 'routes', vote, message: `Voting in "${vote}"` })
    appInsights.trackEvent({ name: 'Vote', properties: { vote } })
    appInsights.trackMetric({ name: 'VoteCount', value: 1 })
    // elasticApm.setCustomContext({ vote })
    // elasticApm.setLabel('label-key', 'vote')
    // elasticApm.registerMetric('vote', () => 1)

    if (vote === 'reset') {
      await Promise.all([
        redis.set(config.BUTTON1, 0),
        redis.set(config.BUTTON2, 0)
      ])
    } else {
      await redis.incr(vote)
    }

    const [value1, value2] = await Promise.all([
      redis.get(config.BUTTON1),
      redis.get(config.BUTTON2)
    ])

    res.render('index', {
      title: config.TITLE,
      button1: config.BUTTON1,
      button2: config.BUTTON2,
      value1,
      value2
    })
  } catch (err) {
    next(err)
  }
})

module.exports = router
