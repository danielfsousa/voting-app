const elasticApm = require('elastic-apm-node')
const appInsights = require('applicationinsights')
const pino = require('pino')
const config = require('./config')

const logger = pino({
  messageKey: 'message',
  level: config.LOG_LEVEL
})

if (config.ELASTIC_APM_SERVER_URL) {
  elasticApm.start({
    captureBody: config.LOG_REQUEST_BODY
  })
}

if (config.APPINSIGHTS_INSTRUMENTATIONKEY) {
  appInsights.setup(config.APPINSIGHTS_INSTRUMENTATIONKEY).start()
}

module.exports = {
  logger,
  elasticApm,
  appInsights: appInsights.defaultClient
}
