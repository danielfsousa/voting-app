const { elasticApm, appInsights } = require('./utils')

const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const redis = require('./redis')
const config = require('./config')
const indexRouter = require('./routes')
const { loggerMiddleware } = require('./middlewares')
const app = express()

app.locals.config = config
app.locals.elasticApm = elasticApm
app.locals.appInsights = appInsights

// view engine setup
app.set('views', path.join(__dirname, '../views'))
app.set('view engine', 'pug')

app.use(loggerMiddleware)
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, '../public')))
app.use(elasticApm.middleware.connect())

app.use('/', indexRouter)

app.use('/healthz', function (req, res, next) {
  if (redis.status === 'ready') {
    res.send({ status: 'ok' })
  } else {
    res.status(500).send({ status: 'redis is not ready' })
  }
})

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // log error
  req.log.error(err)
  appInsights.trackException({ exception: err })
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}
  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
